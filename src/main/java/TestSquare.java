/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author akemi
 */
public class TestSquare {
    
    public static void main(String[] args) {
        Square square1 = new Square(2);
        System.out.println("Area of square1 (n = " + square1.getN() + ") is " + square1.squArea());
        square1.setN(4);
        System.out.println("Area of square1 (n = " + square1.getN() + ") is " + square1.squArea());
        square1.setN(0);
        System.out.println("Area of square1 (n = " + square1.getN() + ") is " + square1.squArea());
    }
}
